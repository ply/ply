;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


;;; Package to import Alias|wavefront OBJ files
;;; Copyright 2008 Charlie McMackin

(defpackage :cl-obj
  (:use #:common-lisp)
  (:shadow #:step)
  (:export #:load-obj-file
	   #:cl-obj
	   #:vertices #:faces #:vertex-normals))


(in-package #:cl-obj)


;;; Lisp-OBJ Spec handler functions

(defclass cl-obj ()
  ((vertices
    :accessor vertices 
    :initform (make-array 100
			  :element-type 'v3d:vector3d
			  :adjustable t :fill-pointer 0))
   (faces
    :accessor faces 
    :initform (make-array 30
			  :element-type '(simple-array fixnum (3))
			  :adjustable t :fill-pointer 0))
   (vertex-normals
    :accessor vertex-normals 
    :initform (make-array 100
			  :element-type 'v3d:vector3d
			  :adjustable t :fill-pointer 0))))

(defparameter *obj* (make-instance 'cl-obj))


#+ignore
(defun v (x y z &optional w)
  (flet ((add-point (point)
	   (vector-push-extend (coerce point 'single-float) *v*)))
    (add-point x)
    (add-point y)
    (add-point z)
    (when w (add-point w))))

(defun v (x y z &optional w)
  (with-slots (vertices) *obj*
    (vector-push-extend (if w (v3d:vec4 x y z w) (v3d:vec3 x y z)) vertices)))

(defun vn (i j k)
  (with-slots (vertex-normals) *obj*
    (vector-push-extend (v3d:vec3 i j k) vertex-normals)))

#+ignore
(defun vp (u &optional v w)
  (vector-push-extend (coerce u 'single-float) *vp*)
  (when v (vector-push-extend (coerce v 'single-float) *vp*))
  (when w (vector-push-extend (coerce w 'single-float) *vp*)))

#+ignore
(defun vt (u &optional v w)
  (vector-push-extend (float u) *vt*)
  (when v (vector-push-extend (float v) *vt*))
  (when w (vector-push-extend (float w) *vt*)))

(defgeneric f (f1 f2 f3 &optional f4))

(defmethod f ((f1 fixnum) (f2 fixnum) (f3 fixnum) &optional f4)
  (with-slots (faces) *obj*
    (symbol-macrolet ((face (if f4
				(vector (1- f1) (1- f2) (1- f3) (1- f4))
				(vector (1- f1) (1- f2) (1- f3)))))
      (vector-push-extend face faces))))

#+ignore
(defmethod f ((f1 float) (f2 float) (f3 float) &optional f4 )
  (with-slots (faces) *obj*
    (symbol-macrolet ((face (if f4
				(v3d:vec4 (1- f1) (1- f2) (1- f3) (1- f4))
				(v3d:vec3 (1- f1) (1- f2) (1- f3)))))
      (vector-push-extend face faces))))

(defmethod f ((f1 list) (f2 list) (f3 list) &optional f4 )
  ;;FIXME: this doesn't handle normal or uv indices.
  (with-slots (faces) *obj*
    (symbol-macrolet ((face (if f4
				(vector (1- (car f1))
					(1- (car f2))
					(1- (car f3))
					(1- (car f4)))
				(vector (1- (car f1))
					(1- (car f2))
					(1- (car f3))))))
      (vector-push-extend face faces))))

;;; _______________________________________________________
;;; Not yet implemented; FIXMEs

(defun not-implemented (&rest args)
  (declare (ignore args))
  (values))

(defun g (&rest args)
  (declare (ignore args))
  (values))

(defmacro deg (&rest args) `(not-implemented ,args))
(defmacro bmat (&rest args) `(not-implemented ,args))
(defmacro step (&rest args) `(not-implemented ,args))
(defmacro cstype (&rest args) `(not-implemented ,args))
(defmacro p (&rest args) `(not-implemented ,args))
(defmacro l (&rest args) `(not-implemented ,args))
(defmacro curv (&rest args) `(not-implemented ,args))
(defmacro curv2 (&rest args) `(not-implemented ,args))
(defmacro surf (&rest args) `(not-implemented ,args))
(defmacro parm (&rest args) `(not-implemented ,args))
(defmacro trim (&rest args) `(not-implemented ,args))
(defmacro hole (&rest args) `(not-implemented ,args))
(defmacro scrv (&rest args) `(not-implemented ,args))
(defmacro sp (&rest args) `(not-implemented ,args))
(defmacro end (&rest args) `(not-implemented ,args))
(defmacro con (&rest args) `(not-implemented ,args))
(defmacro s (&rest args) `(not-implemented ,args))
(defmacro mg (&rest args) `(not-implemented ,args))
(defmacro o (&rest args) `(not-implemented ,args))
(defmacro bevel (&rest args) `(not-implemented ,args))
(defmacro c_interp (&rest args) `(not-implemented ,args))
(defmacro d_interp (&rest args) `(not-implemented ,args))
(defmacro lod (&rest args) `(not-implemented ,args))
(defmacro usemtl (&rest args) `(not-implemented ,args))
(defmacro mtllib (&rest args) `(not-implemented ,args))
(defmacro shadow_obj (&rest args) `(not-implemented ,args))
(defmacro trace_obj (&rest args) `(not-implemented ,args))
(defmacro ctech (&rest args) `(not-implemented ,args))
(defmacro stech (&rest args) `(not-implemented ,args))
;;;------------------------------------------------------


(defun face-normals ()
  (declare (optimize (speed 3) (space 0)))
  (with-slots (vertices faces) *obj*
    (let* ((len (length faces))
	   (face-norms (make-array len
				   :element-type 'v3d:vector3d
				   :initial-element (v3d:vec3-0))))
      (loop 
	 for i below len
	 for face across faces
	 for vert1 = (aref vertices (aref face 0))
	 for vert2 = (aref vertices (aref face 1))
	 for vert3 = (aref vertices (aref face 2))
	 do (setf (aref face-norms i) (v3d:unit-normal-tri! vert1 vert2 vert3)))
      face-norms)))


(defun set-vertex-normals ()
  "Sets vertex normals for files that don't have them"
  (declare (optimize (speed 3) (space 0)))
  (with-slots (vertex-normals vertices faces) *obj*
    (setf vertex-normals (make-array (length vertices)
				     :element-type 'v3d:vector3d
				     :initial-element (v3d:vec3-0)))
    (loop
       :with fnorms = (face-normals)
       :for norm :of-type v3d:vector3d :across fnorms ;step the f-norm w/ face
       :for face :across faces
       :do (loop :for i :of-type fixnum :across face
	      ;; I don't know why vector+! won't work here yet
	      :do (setf (aref vertex-normals i)
			(v3d:vector+ (aref vertex-normals i) norm))))      

    ;;normalize everything
    (loop :for norm :of-type v3d:vector3d
       :across vertex-normals :do (v3d:normalize! norm))))

;(map-into vertex-normals #'v3d:normalize! vertex-normals)

#+Ignore
(defun set-face-normals ()
  (loop for normal in (face-normals *v* *f*)
       do (mapcar #'(lambda (x) (vector-push-extend x *vn*))
		  normal)))

#+ignore
(defun set-face-normals ()
  (loop for (x y z) in (face-normals *v* *f*) 
     do (vn x y z)))


;; OBJ file loader

(defun obj-string-to-sexp (string)
  (declare (optimize (speed 3) (space 0)))
  "Turns a line from an OBJ file into a symbolic expression"
  (flet ((parse-f (string)
	   "Extra formatting for f commands"
	   (when (char= (char string 0) #\f)
	     (nsubstitute #\Space #\/ string))
	   string))
    (when string
      (let ((trimmed-line
	     (string-trim '(#\Space #\Return #\Tab #\Newline) string)))
	(declare (type string trimmed-line))
	(if (or (zerop (length trimmed-line)) 
		(char= (char trimmed-line 0) #\#))
	    ""
	    (concatenate 'string "(:" (parse-f trimmed-line) ")"))))))


(defun parse-commands (exp)
  (declare (optimize (speed 3) (space 0)))
  ;;; This feels so dirty. But I can think of no other
  ;;; way for it to work when called outside of the package. :(
  (flet ((parse-f (list)
	   (case (list-length list)
	     (6 (loop for (a b) on list by #'cddr
		   collect (list a b)))
	     (9 (loop for (a b c) on list by #'cdddr
		   collect (list a b c)))
	     (t list))))
    (let ((args (rest exp)))
      (case (first exp)
	(:v (apply #'v args))
	(:f (apply #'f (parse-f args)))
	(:vn (apply #'vn args))
	(:g (apply #'g args))))))


(defun load-obj-file (obj-path &key (normals t))
  "Load an OBJ file into a CL-OBJ"
  (let ((*obj* (make-instance 'cl-obj)))
    (with-open-file (in obj-path :direction :input) 
      (loop
	 :for line = (obj-string-to-sexp (read-line in nil))
	 :while line
	 :for s-exp = (read-from-string line nil)
	 :do (when s-exp (parse-commands s-exp))))
    (when (and normals
	       (/=  (length (vertex-normals *obj*)) (length (vertices *obj*))))
      (set-vertex-normals))
   *obj*))