;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


; (declaim (optimize (debug 3) (safety 3) (speed 0)))

(in-package #:ply)

(defclass gl-window (glut:window)
  ((view-camera :accessor view-camera 
		:initarg :view-camera)
   (save-camera :accessor save-camera 
		:initform (make-instance 'interactive-camera 
					 :aspect (/ 640 480))))
  
  (:default-initargs :title "ply" :width 640 :height 480
		     :mode '(:double :rgb :depth :alpha)
		     :view-camera (make-instance 'camera :aspect (/ 640 480))))


(defmethod glut:display-window :before ((w gl-window))
;;; Create menu and attach to right mouse button
  (let ((menu (list "Move" "Rotate" "Scale" "One Light" "Two Lights" "Quit")))
    (menu-on-button 2 menu))

;;; Init OpenGL Scene
  (gl:shade-model :flat)
;  (gl:front-face :ccw)
  (gl:enable :depth-test :lighting :color-material)
  (set-lighting)
  ;  (gl:light-model :light-model-ambient #(0.1 0.1 0.1 1.0))
  
  (gl:color-material :front :ambient-and-diffuse)
  
  (gl:material :front :specular #(1.0 1.0 1.0 1.0))
  (gl:material :front :shininess 28))

(defmethod glut:display ((w gl-window))
  (gl:clear-color .8 .8 .8 1)
  (gl:clear :color-buffer :depth-buffer)

  (with-slots ((cam view-camera)) w
    (view-from-camera cam)
    (draw-grid 2)	    
    (draw *background*)
    (draw *scene*))
  (glut:swap-buffers))


(defmethod glut:reshape ((w gl-window) width height)
  (gl:viewport 0 0 width height)
  (with-slots ((cam view-camera)) w
    (setf (aspect cam)  (float (/ width height) 1d0)
	  (glut:width w) width
	  (glut:height w) height)
    (cam-projection cam)))

(defun start (&optional path)
  (when path 
    (add-to-scene *scene* (import-obj path)))
  (let ((instance (make-instance 'gl-window)))
    
    #+(and sbcl sb-thread)
    (sb-thread:make-thread (lambda ()
			     (glut:display-window instance)))
    #-sb-thread
    (glut:display-window instance)))

;;; Used for executable cores

(defun start-core ()
  (let ((*features* (remove :sb-thread *features*)))
    (start))
  #+sbcl (sb-ext:quit :unix-status 0))

(defun save-core ()
  #+sbcl (sb-ext:save-lisp-and-die "ply.exe"
				   :executable t
				   :toplevel 'ply::start-core))