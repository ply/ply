;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;

;; Classes intended to help manage Vertex Arrays (vao) and Vertex Buffer Objects
;; (vbo)in OpenGL up to version 2.1 at the time of writing. Uses cl-opengl

;; idea: define a VA class and then allow a VBO class to be a subclass ...or vice
;; versa?

(in-package #:ply)

(export '(vao
	  vbo
	  make-vao
	  make-vbo
	  size
	  stride
	  gl-type
	  data
	  buffer
	  data-buffer
	  target
	  usage
	  enable-vao
	  draw-elements))

(declaim (optimize (speed 3) (safety 1)))

(defclass vao ()
  ((size      :initarg :size
	      :accessor size)

   (stride    :initarg :stride
	      :accessor stride)
   
   (gl-type   :initarg :gl-type 
	      :accessor gl-type)

   (data      :initarg :data
	      :accessor data)

   (elt-count :initarg :elt-count
	      :reader elt-count))
  (:default-initargs :data (null-pointer))
  (:documentation "A Vertex Array Object 
DATA receives a flat Lisp array or list
GL-TYPE receives the cl-opengl gl-type keyword such as :FLOAT :INT etc"))

(defclass vbo (vao)
  ((buffer      :accessor buffer
		:initarg :buffer)

   (data-buffer :accessor data-buffer
		:initarg :data-buffer)

   (target      :accessor target 
	        :initarg :target)

   (usage       :accessor usage
		:initarg :usage)

   (bound       :accessor bound
		:initarg :bound))
  (:default-initargs :buffer (first (gl:gen-buffers 1)) :bound nil)
  (:documentation "A Vertex Buffer Object
BUFFER should be a buffer number from (gl:genbuffers)
TARGET receives a buffer target keyword such as :array-buffer
USAGE receives a keyword such as :stream-draw"))

(defun make-vao (size stride gl-type vertices)
  (make-instance 'vao
		 :size size
		 :stride stride
		 :gl-type gl-type
		 :data (cffi:foreign-alloc gl-type :initial-contents vertices)
		 :elt-count (length vertices)))

(defun make-vbo (size stride gl-type target vertices usage)
  (make-instance 'vbo
		 :size size
		 :data-buffer (cffi:foreign-alloc gl-type :initial-contents vertices)
		 :elt-count (length vertices)
		 :stride stride
		 :gl-type gl-type
		 :target target
		 :usage usage))
;;; Generics

(defgeneric enable-vao (fn-name vao)
  (:documentation
   "Calls %gl:*-pointer based on FN-NAME symbol. If it is a VBO, buffers
are bound beforehand. "))

(defgeneric draw-elements (mode vao))
(defgeneric draw-elements (mode vbo))

;;; Methods
(defmethod enable-vao (fn-name (v vao))
  (with-slots (size stride data)  v
    (let ((typex (gl::cffi-type-to-gl (gl-type v))))
     (case fn-name 
       (:vertex          (%gl:vertex-pointer size typex stride data))
       (:normal          (%gl:normal-pointer typex stride data))
       (:color           (%gl:color-pointer size typex stride data))
       (:secondary-color (%gl:secondary-color-pointer size typex stride data))
       (:index           (%gl:index-pointer typex stride data))
       (:edge-flag       (%gl:edge-flag-pointer stride data))
       (:fog-coord       (%gl:fog-coord-pointer typex stride data))
       (:tex-coord       (%gl:tex-coord-pointer size typex stride data))
       (:vertex-attrib)
       (t ())))))

(defmethod enable-vao :before (fn-name (v vbo))
  (declare (ignore fn-name))
  (with-slots (elt-count gl-type target buffer data-buffer usage) v
    (let ((byte-size (* (cffi:foreign-type-size gl-type) elt-count)))
      (if (bound v)
	  (%gl:buffer-data target byte-size data-buffer usage)
	  (progn (%gl:bind-buffer target buffer)
		 (setf (bound v) t)
		 (%gl:buffer-data target byte-size data-buffer usage))))))


(defmethod enable-vao :after (fn-name (v vbo))
  (declare (ignore fn-name))
  (cffi:foreign-free (data-buffer v)))


(defmethod draw-elements (mode (v vao))
  (%gl:draw-elements mode (elt-count v) (gl::cffi-type-to-gl (gl-type v)) (data v)))

(defmethod draw-elements :before (mode (v vbo))
  (unless (bound v)
    (%gl:bind-buffer (target v) (buffer v))
    (setf (bound v) t)))

