;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)


(defclass camera ()
  ((target        :initarg :target    :accessor target :type v3d:vector3d)
   (near          :initarg :near      :accessor near :type double-float)
   (far           :initarg :far       :accessor far :type double-float)
   (field-of-view :initarg :fov       :accessor fov :type double-float)
   (aspect        :initarg :aspect    :accessor aspect :type double-float)
   (pan-x         :initarg :pan-x     :accessor pan-x)
   (pan-y         :initarg :pan-y     :accessor pan-y)
   (distance      :initarg :distance  :accessor distance)
   (azimuth       :initarg :azimuth   :accessor azimuth)
   (elevation     :initarg :elevation :accessor elevation)
   (along-axis    :initarg :along     :accessor along-axis)
   (ortho-mode    :initarg :ortho-mode    :accessor ortho-mode)
   (movingp       :initform nil       :accessor movingp)
   (camera-mode   :initarg :mode      :accessor camera-mode))
  (:default-initargs :target (v3d:vec3-0)
    :near 0.1d0 :far 10000d0 :fov 45d0 :pan-x 0.0 :pan-y 0.0
    :along nil :distance 8.0 :azimuth -45.0 :elevation 25.0
    :ortho-mode 'persp :mode 'persp))

(defclass interactive-mixin ()
  ((click-x :accessor click-x :initarg :cx :type fixnum)
   (click-y :accessor click-y :initarg :cy :type fixnum))
  (:default-initargs :cx 0 :cy 0))

(defclass interactive-camera (interactive-mixin camera)
  ()
  (:documentation "Mix-in to provide slots for mouse interation to a CAMERA"))

(defun cam-projection (camera)
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (with-slots (aspect distance (fov field-of-view) near far ortho-mode)
      camera
    (ecase ortho-mode
      (ortho (let* ((sz (* distance (tan (* fov #.(/ pi 360)))))
		    (-sz (- sz)))
	       (gl:ortho (* -sz aspect) (* sz aspect) -sz sz near far)))
      (persp (glu:perspective fov aspect near far))))
  (gl:matrix-mode :modelview))

#+ignore
(defun view-from-camera (camera)
  (declare (optimize (speed 3)))
  (with-slots (target distance azimuth elevation pan-x pan-y) 
      camera
    (declare (type v3d:vector3d target)
	     (single-float distance azimuth elevation pan-x pan-y))
  
    (gl:load-identity)
;; How can I use glu:look-at here?
    (let ((target-x (aref target 0))
	  (target-y (aref target 1))
	  (target-z (aref target 2)))
      (let ((move-x (- target-x pan-x))
	    (move-y (- target-y pan-y))
	    (move-z (+ target-z distance)))
	(declare (single-float target-x target-y target-z
			       move-x move-y move-z))
	(glu:look-at move-x move-y move-z
		     target-x target-y target-z
		     0.0 1.0 0.0) 
      ;; (gl:translate pan-x pan-y (- distance))
	(gl:rotate elevation 1.0 0.0 0.0)
	(gl:rotate azimuth 0.0 1.0 0.0)
;	(gl:translate move-x move-y move-z)
	))))

(defun view-from-camera (camera)
  (declare (optimize (speed 3)))
  (with-slots (target distance azimuth elevation pan-x pan-y) 
      camera
    (declare (type v3d:vector3d target)
	     (single-float distance azimuth elevation pan-x pan-y))
  
    (gl:load-identity)
    ;; How can I use glu:look-at here?
     
    (gl:translate pan-x pan-y (- distance))
    (gl:rotate elevation 1.0 0.0 0.0)
    (gl:rotate azimuth 0.0 1.0 0.0)
	;	(gl:translate move-x move-y move-z)
    ))

(defun toggle-ortho (camera)
  (setf (ortho-mode camera) (if (eql (ortho-mode camera) 'persp)
				'ortho
				'persp))
  (cam-projection camera)
  (view-from-camera camera)
  
  (glut:post-redisplay))

(defun set-camera (cam1 cam2)
  (with-slots (elevation azimuth pan-x pan-y) cam1
    (with-slots ((ev2 elevation) (az2 azimuth) (x2 pan-x) (y2 pan-y)) cam2
      (setf elevation ev2
	    azimuth az2
	    pan-x x2
	    pan-y y2))))

(defun zoom (camera zoom)
  (with-slots (ortho-mode) camera
    (ecase ortho-mode
      (ortho (dolly-zoom camera zoom))
      (persp (ortho-zoom camera zoom)))
    (glut:post-redisplay)))

(defun dolly-zoom (camera zoom)
;; decreasing distance brings cam closer to focal point
  ;  (decf (aref (target camera) 2) zoom)
  (decf (distance camera) zoom))

(defun ortho-zoom (camera zoom)
  (incf (fov camera) zoom))

(defun toggle-mode (camera)
  (setf (camera-mode camera) 
	(if (eql (camera-mode camera) 'track)
	    'orbit 
	    'track)))

(defun radians-to-degrees (radian)
  (symbol-macrolet ((f-pi (float pi 1.0)))
    (* (/ 180 f-pi) radian)))

(defun degrees-to-radians (degree)
  (symbol-macrolet ((f-pi (float pi 1.0)))
    (* (/ f-pi 180) degree)))


;; methods that change polar coords to cartesian
;;
#||
From Nico on Opengl.org

This is exactly what the gluLookAt function is for.

gluLookAt( camX, camY, camZ, orgX, orgY, orgZ, 0.0, 1.0, 0.0 )
with

R = distance*cos(beta)
camX = R*cos(alpha)+orgX
camY = distance*sin(beta)+orgY
camZ = R*sin(alpha)+orgZ

||#
;; elevation is beta; azimuth is alpha
(defmethod pos-x ((cam camera))
  (with-slots ((d distance) (beta elevation) (alpha azimuth)) cam
    (symbol-macrolet ((r (* d (cos (degrees-to-radians beta)))))
      (+ (* r (cos (degrees-to-radians alpha))) (aref (target cam) 0)))))

(defmethod pos-y ((cam camera))
  (with-slots ((d distance) (beta elevation) (alpha azimuth)) cam
    (+ (* d (sin (degrees-to-radians beta))) (aref (target cam) 1))))

(defmethod pos-z ((cam camera))
  (with-slots ((d distance) (beta elevation) (alpha azimuth)) cam
    (symbol-macrolet ((r (* d (cos (degrees-to-radians beta)))))
      (+ (* r (sin (degrees-to-radians alpha))) (aref (target cam) 2)))))

(defmethod track ((vcam camera) (scam interactive-camera) x y)
  (with-slots (target) vcam
    (with-slots ((new-target target) click-x click-y) scam
      (symbol-macrolet ((new-x (+ (aref new-target 0) (* (- click-x x) 0.025)))
			(new-y (+ (aref new-target 1) (* (- y click-y) 0.025))))
       (setf (aref target 0) new-x	     
	     (aref target 1) new-y)))))

(defmethod pan ((vcam camera) (scam interactive-camera) x y)
  (with-slots (pan-x pan-y) vcam
    (with-slots ((ox pan-x) (oy pan-y) click-x click-y) scam
      (declare (single-float pan-x pan-y ox oy))
      (symbol-macrolet ((dx (+ ox (* (- click-x x) 0.025)))
			(dy (+ ox (* (- y click-y) 0.025))))
	(setf pan-x dx)
	(setf pan-y dy)))))

(defmethod orbit ((vcam camera) (scam interactive-camera) x y)
  (setf (elevation vcam) 
	(+ (elevation scam) (- (click-y scam) y))
	(azimuth vcam) 
	(+ (azimuth scam) (- (click-x scam) x))))

#+ignore
(defun get-pick-ray (x y)
  (let* ((ray-begin (multiple-value-list (glu:un-project x y 0.0)))
	  (ray-end (multiple-value-list (glu:un-project x y 1.0)))
	  (diff (mapcar #'- ray-end ray-begin))
	  (len (sqrt (reduce #'+ (mapcar #'(lambda (q)(* q q)) diff)))))
    (when (> len 0)
      (format t "~&Pick Ray Vector: ~A"
	      (mapcar #'(lambda (n) (* (/ 1 len) n)) diff)))))

#+ignore
(defun get-pick-ray2 (x y camera)
;; this version uses the camera position.
  (multiple-value-bind (ox oy oz)
      (glu:un-project x y 0.0)
    (multiple-value-bind (ex ey ez)
	(glu:un-project x y 1.0)
      (let ((origin (v3d:vec3 (pos-x camera) (pos-y camera) (pos-z camera)))
	    (direction (v3d:normalize! (v3d:vector- 
					(v3d:vec3 ex ey ez) 
					(v3d:vec3 ox oy (- oz))))))
	(format t "~&Ray ori: ~a  dir: ~a~%" origin direction)
	(make-ray origin direction)))))

(defun get-pick-ray2 (x y camera)
  ;; this version doesn't use the camera position, just the x, y click.
  ;  (declare (ignore camera))
  (multiple-value-bind (ox oy oz)
      (glu:un-project x y 0.0)
    (multiple-value-bind (ex ey ez)
	(glu:un-project x y 1.0)
      (let ((origin (v3d:vec3 ox oy oz))
	    (end (v3d:vec3 ex ey ez)))
	(format t "~&Ray ori: ~a  dir: ~a
camera X: ~a Y: ~a Z: ~a~%"
		origin end (pos-x camera) (pos-y camera) (pos-z camera))
	(make-ray-from-line origin end)))))

(defun get-pick-ray3 (x y camera)
  ;; this version doesn't use the camera position, just the x, y click.
  ;  (declare (ignore camera))
  (let ((modelview-matrix (gl:get-double :modelview-matrix))
	(projection-matrix (gl:get-double :projection-matrix))
	(viewport (gl:get-integer :viewport)))
    (multiple-value-bind (ox oy oz)
	(glu:un-project x y 0.0 
			:modelview modelview-matrix 
			:projection projection-matrix 
			:viewport viewport)
      (multiple-value-bind (ex ey ez)
	  (glu:un-project x y 1.0 
			  :modelview modelview-matrix 
			  :projection projection-matrix 
			  :viewport viewport)
	(let ((origin (v3d:vec3 ox oy oz))
	      (end (v3d:vec3 ex ey ez)))
	  #+ignore
	  (format t "~&Ray ori: ~a  dir: ~a
camera X: ~a Y: ~a Z: ~a~%"
		  origin end (pos-x camera) (pos-y camera) (pos-z camera))
	  (make-plucker-ray-from-line origin end))))))



