;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;

(in-package #:ply)

(defun basic-tri-draw (vertex-array face-index-array)
  (gl:with-primitives :triangles 
    (loop for face across face-index-array 
       do (loop for index across face
	     for vertex = (aref vertex-array index)
	     for x = (aref vertex 0)
	     for y = (aref vertex 1)
	     for z = (aref vertex 2)
	     do (gl:vertex x y z)))))

(defun basic-tri-smooth-draw (vertex-array face-index-array vertex-normal-array)
  (gl:with-primitives :triangles 
    (loop for face across face-index-array 
       do (loop for index across face
	     for vertex = (aref vertex-array index)
	     for normal = (aref vertex-normal-array index)
	     for x = (aref vertex 0)
	     for y = (aref vertex 1)
	     for z = (aref vertex 2)
	     for nx = (aref normal 0)
	     for ny = (aref normal 1)
	     for nz = (aref normal 2)
	     do (gl:normal nx ny nz)
	     do (gl:vertex x y z)))))




;; ------- Draw Methods --------
(defmethod draw ((obj list))
  (dolist (drawable obj)
    (draw drawable)))

(defmethod draw ((obj transform))
  (values))

(defmethod draw :before ((obj transform))
  (gl:push-matrix)
  (with-slots (translation rotation scale)
      obj
    (symbol-macrolet ((tx (aref translation 0))
		      (ty (aref translation 1))
		      (tz (aref translation 2))
		      (rx (aref rotation 0))
		      (ry (aref rotation 1))
		      (rz (aref rotation 2))
		      (sx (aref scale 0))
		      (sy (aref scale 1))
		      (sz (aref scale 2)))
      (gl:translate tx ty tz)
      (gl:rotate rx 0 0 1)
      (gl:rotate ry 0 1 0)
      (gl:rotate rz 1 0 0)
      (gl:scale sx sy sz))))

(defmethod draw :after ((obj transform))
  (gl:pop-matrix))

(defmethod draw ((obj object-state))
  (values))

(defmethod draw :around ((obj object-state))
  (unless (hiddenp obj)
    (call-next-method)))

(defmethod draw :before ((obj object-state))
  (gl:enable :polygon-offset-fill))


(defmethod draw :after ((obj object-state))
  (gl:disable :polygon-offset-fill))

(defmethod draw ((obj standard-mesh))
  (when (solidp obj)
    (gl:enable :lighting)
    (gl:polygon-offset 1 1)
    (gl:polygon-mode :front-and-back :fill)
    (if (highlightp obj)
	(gl:color 0.8 0.8 0.3)
	(gl:color 0.8 0.4 0.2))    
    (with-slots (face-indices vertex-array vertex-normals) obj
      (if (plusp (length vertex-normals))
	  (basic-tri-smooth-draw vertex-array face-indices vertex-normals)
	  (basic-tri-draw vertex-array face-indices))))
  (when (wireframep obj)
    (gl:disable :lighting)
    (gl:polygon-offset 0 0)
    (gl:polygon-mode :front-and-back :line)
    (if (highlightp obj)
	(gl:color 0.8 0.8 0.3)
	(gl:color 0.0 0.0 0.0))
    (with-slots (face-indices vertex-array) obj
      (basic-tri-draw vertex-array face-indices))))


(defmethod draw :before ((obj vbo-mesh))
  (with-slots (vertex-vbo norm-vbo index-vbo) obj
    (enable-vao :vertex vertex-vbo)
    (enable-vao :normal norm-vbo)
    (enable-vao :index index-vbo)))

(defmethod draw ((obj vbo-mesh))
  (gl:enable-client-state :vertex-array)
  (gl:enable-client-state :normal-array)
  (with-slots (index-vbo) obj
    (when (solidp obj)
      (gl:enable :lighting)
      (gl:polygon-offset 1 1)
      (gl:polygon-mode :front-and-back :fill)
      (if (highlightp obj)
	  (gl:color 0.8 0.8 0.3)
	  (gl:color 0.8 0.4 0.2))
      (draw-elements :triangles index-vbo))
    (when (wireframep obj)
      (gl:disable :lighting)
      (gl:polygon-offset 0 0)
      (gl:polygon-mode :front-and-back :line)
      (if (highlightp obj)
	  (gl:color 0.8 0.8 0.3)
	  (gl:color 0.0 0.0 0.0))
      (draw-elements :triangles index-vbo)))
  (gl:disable-client-state :normal-array)
  (gl:disable-client-state :vertex-array))


(defmethod draw :before ((light simple-light))
  (gl:enable (light-num light)))

(defmethod draw ((light simple-light))
  (with-slots (light-num diffuse ambient specular) light
    (cond (diffuse  (gl:light light-num :diffuse diffuse))
	  (ambient  (gl:light light-num :ambient ambient))
	  (specular (gl:light light-num :specular specular))))
					;  (gl:light-model :light-model-ambient (ambient light))
  )

(defmethod draw :after ((light point-light))
  (with-slots (light-num (position light-position))
      light
    (gl:light light-num :position position)))


(defmethod draw :after ((light directional-light))
  (with-slots (light-num (direction light-direction))
      light
    ;; direction should have last element of 0.0
    (gl:light light-num :position direction)))



#+ignore
(defmethod draw ((obj cl-obj::cl-obj))
  (gl:enable :lighting)
  (gl:with-pushed-matrix 
    (gl:translate 0 0 0)
    (gl:enable :polygon-offset-fill)
    (gl:polygon-offset 1 1)
    (gl:polygon-mode :front-and-back :fill)
    (gl:color 0.8 0.4 0.2)
    
    (with-slots ((faces cl-obj::faces) (verts cl-obj::vertices)) obj
      (gl:with-primitives :triangles 
	(loop for face across faces 
	   do (loop for index across face
		 for vertex = (aref verts index)
		 for x = (aref vertex 0)
		 for y = (aref vertex 1)
		 for z = (aref vertex 2)
		 do (gl:vertex x y z)))))
    (gl:disable :polygon-offset-fill)))



;;;----------------------------------------------------


(defmethod toggle-wireframe ((obj object-state))
  (setf (wireframep obj) (not (wireframep obj))))

(defmethod toggle-wireframe ((list list))
  (dolist (obj list)
    (toggle-wireframe obj)))

(defmethod toggle-wireframe ((obj t))
  (values))

(defmethod toggle-wireframe :after ((obj t))
  (declare (ignore obj))
  (glut:post-redisplay))

(defmethod toggle-solid ((list list))
  (dolist (obj list)
    (toggle-solid obj)))

(defmethod toggle-solid ((obj object-state))
  (setf (solidp obj) (not (solidp obj)))
  (glut:post-redisplay))

(defmethod toggle-solid ((obj t))
  (values))

(defmethod toggle-solid :after ((obj t))
  (declare (ignore obj))
  (glut:post-redisplay))

(defmethod toggle-hide ((obj object-state))
  (setf (hiddenp obj) (not (hiddenp obj))))

(defmethod toggle-hide ((obj t))
  (values))

(defmethod toggle-hide :after ((obj t))
  (declare (ignore obj))
  (glut:post-redisplay))

(defmethod toggle-highlight ((obj object-state))
  (setf (highlightp obj) (not (highlightp obj))))
(defmethod toggle-highlight ((obj t))
  (values))
(defmethod toggle-highlight :after ((obj t))
  (declare (ignore obj))
  (glut:post-redisplay))






(defmethod translate ((obj transform) x y z)
  (with-slots (translation) obj
    (setf (aref translation 0) x
	  (aref translation 1) y
	  (aref translation 2) z)))

(defun draw-grid (size)
  (flet ((draw-lines (n width)
	   (gl:vertex 0 0 n)
	   (gl:vertex width 0 n)
	   (gl:vertex n 0 0)
	   (gl:vertex n 0 width)))
    (gl:disable :lighting)
    (gl:line-width 1)
    (gl:color .5 .5 .5)
    (gl:with-pushed-matrix
	(gl:translate (- (* 5 size)) 0 (- (* 5 size)))
      (gl:with-primitives :lines
	(loop with width = (* 10 size) 
	   for i upto width
	   do (draw-lines i width))))
    (gl:enable :lighting))
)











#+ignore (macrolet ((vref (n) `(aref (aref verts index) ,n)))
		      (gl:vertex (vref 0) (vref 1) (vref 2))
		      )