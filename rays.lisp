;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)

(defclass ray3d ()
  ((origin :accessor origin :initarg :origin :type v3d:vector3d)
   (direction :accessor direction :initarg :direction :type v3d:vector3d)))

#+ignore
(defclass plucker-ray (ray3d)
  ((category :accessor category :initarg :category)
   (p-coefficients :accessor p-coefficients :initarg :p-co :type v3d:vector3d)
   (inverse-dir :accessor inv-dir :initarg :inv-dir :type v3d:vector3d)))

(defclass plucker-ray (ray3d)
  ((category :accessor category :initarg :category)
   (r0 :accessor r0 :type single-float)
   (r1 :accessor r1 :type single-float)
   (r3 :accessor r3 :type single-float)
   (inverse-dir :accessor inverse-dir :type v3d:vector3d)))


  
(defmethod initialize-instance :after ((p-ray plucker-ray) &key origin direction &allow-other-keys)
  (with-slots (category r0 r1 r3 inverse-dir) p-ray
    (let ((dx (aref direction 0))
	  (dy (aref direction 1))
	  (dz (aref direction 2))
	  (x (aref origin 0))
	  (y (aref origin 1))
	  (z (aref origin 2)))
      
      (setf inverse-dir (v3d:inverse direction)
	    r0 (- (* x dy) (* dx y))
	    r1 (- (* x dz) (* dx z))
	    r3 (- (* y dz) (* dy z)))
      
      (setf category (if (minusp dx)
			 (if (minusp dy)
			     (if (minusp dz)
				 'MMM
				 'MMP)
			     (if (minusp dz)
				 'MPM
				 'MPP))
			 (if (minusp dy)
			     (if (minusp dz)
				 'PMM
				 'PMP)
			     (if (minusp dz)
				 'PPM
				 'PPP)))))))

(defmethod draw ((ray ray3d))
  (with-slots ((orig origin) (dir direction)) ray
    ;; find end point at time 10 from orig
    (let ((end (v3d:vector+ orig (v3d:vector-s* dir 10.0))))
      (gl:disable :lighting)
      (gl:line-width 1.3)
      (gl:color 0.9 0.0 0.0)
      (gl:with-primitives :lines
	(gl:vertex (aref orig 0) (aref orig 1) (aref orig 2))
	(gl:vertex (aref end 0) (aref end 1) (aref end 2))))))

(defmethod draw :before ((ray ray3d))
  (gl:push-matrix))
(defmethod draw :after ((ray ray3d))
  (gl:pop-matrix))

(defun make-ray (origin direction)
  (declare (type v3d:vector3d origin direction))
  (let ((ray (make-instance 'ray3d :origin origin :direction direction)))
    ; for visual debugging    (push ray *background*)
    ray))

(defun make-plucker-ray (origin direction)
  (declare (type v3d:vector3d origin direction))
  (let ((ray (make-instance 'plucker-ray :origin origin :direction direction)))
    ; for visual debugging    (push ray *background*)
    ray))

(defun make-ray-from-line (start end)
  (declare (type v3d:vector3d start end)
	   (inline make-ray))
  (make-ray start (v3d:normalize! (v3d:vector- end start))))

(defun make-plucker-ray-from-line (start end)
  (declare (type v3d:vector3d start end)
	   (inline make-plucker-ray))
  (make-plucker-ray start (v3d:normalize! (v3d:vector- end start))))


#+ignore
(defun ray-equal (ray1 ray2)
  (declare (type ray3d ray1 ray2))
  (and (equalp (origin ray1) (origin ray2))
       (equalp (direction ray1) (direction ray2))))


(defgeneric intersect-p (object-1 object-2)
  (:documentation "Test whether object-1 intersects object-2."))



(defmethod intersect-p ((ray1 ray3d) (ray2 ray3d))
  (with-slots ((o1 origin) (d1 direction)) ray1
    (with-slots ((o2 origin) (d2 direction)) ray2
      (let* ((d1xd2 (v3d:cross-product d1 d2))
	     (denom (v3d:mag-sq d1xd2)))
	(if (zerop denom)
	    nil				; rays are parallel
	    (let* ((o2-o1 (v3d:vector- o2 o1))
		   (t1 (/ (v3d:determinant o2-o1 d2 d1xd2) denom))
		   (t2 (/ (v3d:determinant o2-o1 d1 d1xd2) denom)))
	      ;; return truth of interection and the points of closest convergence
	      ;; if T the points are equal
	      (values (equalp t1 t2)
		      (v3d:vector+ o1 (v3d:vector-s* d1 t1))
		      (v3d:vector+ o2 (v3d:vector-s* d2 t2)))))))))

#+ignore
(defun ray-box-intersect2 (ori dir bmin bmax)
;;; Woo's algorithm but modified from the Graphics Gems I implementation
;;; FIXME: algorithm implementation not quite right.
;;; TODO: make more lispy -> mismatch?
  (let ((intersect (v3d:vec3-0))
	(maxt (v3d:vec3 -1.0 -1.0 -1.0)))
    (loop 
       for o across ori
       for d across dir
       for min across bmin
       for max across bmax
       for i below 3
       with inside = t
       do (cond ((< o min)
		 (setf (aref intersect i) min
		       inside nil)
		 (when (not (zerop d))
		   (setf (aref maxt i) (/ (- min o) d))))
		((> o max)
		 (setf (aref intersect i) max
		       inside nil)
		 (when (not (zerop d))
		   (setf (aref maxt i) (/ (- max o) d)))))
       finally
       (if inside
	   (return ori)			;origin is inside the box
	   ;; Get the maximum maxt for final intersection test
	   (return 
	     (loop 
					;		      initially  (format t "~&first intersect test~%")
		for j from 1 below 3
		for max-pos = 0
		when (< (aref maxt max-pos) (aref maxt j))
		do (setf max-pos j)
		finally 
		;; Check final candidate actually inside box
		  (if (minusp (aref maxt max-pos))
		      (return nil)
		      (return 
			(loop 
					;				 initially  (format t "~&second intersect test~%")
			   for k below 3
			   for o across ori
			   for mt across maxt
			   for d across dir
			   for min across bmin
			   for max across bmax
			   when (not (= k max-pos))
			   do (progn (setf (aref intersect k) (+ o (* mt d)))
				     (if (or (< (aref intersect k) min)
					     (> (aref intersect k) max))
					 (return nil)
					 (return intersect))))))))))))


(defun ray-box-intersect (origin direction min-bound max-bound)
;;; This version uses the algorithm a link http://www.realtimerendering.com/int
;;; a revision of Woo's algorithm
  (declare (type v3d:vector3d origin direction min-bound max-bound)
	   (optimize (speed 3) (space 0) (debug 0)))
  (let ((intersect (v3d:vec3-0))
	(max-t (v3d:vec3 -1.0 -1.0 -1.0))
	(inside-p t)
	(which-plane 0))
    (declare (type v3d:vector3d intersect max-t)
	     (type fixnum which-plane))
    (flet ((find-candidate-plane (i)
	     (declare (fixnum i))
	     (let ((orig (aref origin i)))
	       (declare (single-float orig))
	       (cond ((< orig (aref min-bound i))
		      (setf (aref intersect i) (aref min-bound i)
			    inside-p nil)
		      (when (not (zerop (aref direction i)))
			(setf (aref max-t i)  
			      (the single-float (/ (the single-float (- (aref min-bound i) orig)) (aref direction i))))))
		     ((> orig (aref max-bound i))
		      (setf (aref intersect i) (aref max-bound i)
			    inside-p nil)
		      (when (not (zerop (aref direction i)))
			(setf (aref max-t i) 
			      (the single-float (/ (the single-float (- (aref max-bound i) orig)) (aref direction i)))))))))
	   (compute-intersect (i)
	     (if (= i which-plane)
		 t
		 (progn (setf (aref intersect i) 
			      (+ (aref origin i) (* (aref max-t i) (aref direction i))))
			(not (or (< (aref intersect i) (aref min-bound i))
				 (> (aref intersect i) (aref max-bound i))))))))
      (find-candidate-plane 0)
      (find-candidate-plane 1)
      (find-candidate-plane 2)
      (if inside-p 
	  origin
	  (progn
	    (loop for i from 1 below 3 
	       when (< (aref max-t which-plane) (aref max-t i))
	       do (setf which-plane i))
              
	    (if (and (compute-intersect 0)
		     (compute-intersect 1)
		     (compute-intersect 2))
		(values t intersect)
		(values nil intersect)))))))

(defun aab-plucker-cff-intersect-p (category origin direction r0 r1 r3 min-bound max-bound)
  ;; Algorithm from JGT
  ;; Only returns T or NIL for intersection
  ;; aaargh! also doesn't work for inside the box test. but curiously opengl signals zero.
  (declare (type v3d:vector3d origin direction min-bound max-bound)
	   (optimize (speed 3) (debug 0)))
  (let ((ox (aref origin 0))
	(oy (aref origin 1))
	(oz (aref origin 2))
	(dx (aref direction 0))
	(dy (aref direction 1))
	(dz (aref direction 2))
	(minx (aref min-bound 0))
	(miny (aref min-bound 1))
	(minz (aref min-bound 2))
	(maxx (aref max-bound 0))
	(maxy (aref max-bound 1))
	(maxz (aref max-bound 2)))
    (declare (single-float ox oy oz dx dy dz r0 r1 r3 minx miny minz maxx maxy maxz))
    ;; we need a couple macros for saving on the typing
    ;; yes it's ugly but the C++ from
    ;; TODO: can the macros be taken further to make more succinct?
    
    (flet ((big-not-or (a b c d e f g h i j k l)
	     (declare (single-float a b c d e f g h i j k l))
	     (not (or (minusp (- (+ r0 (* dx a)) (* dy g)))
		      (plusp  (- (+ r0 (* dx b)) (* dy h)))
		      (plusp (- (+ r1 (* dx c)) (* dz i)))
		      (minusp (- (+ r1 (* dx d)) (* dz j)))
		      (minusp (+ (- r3 (* dz e)) (* dy k)))
		      (plusp (+ (- r3 (* dz f)) (* dy l)))))))
      (declare (inline big-not-or))
      (case category
	(MMM (if (or (< ox minx) (< oy miny) (< oz minz))
		 nil
		 (big-not-or miny maxy maxz minz maxy miny maxx minx minx maxx minz maxz)))
	(MMP (if (or (< ox minx) (< oy miny) (> oz maxz))
		 nil
		 (big-not-or miny maxy maxz minz miny maxy maxx minx maxx minx minz maxz)))
	(MPM (if (or (< ox minx) (> oy maxy) (< oz minz))
		 nil
		 (big-not-or miny maxy maxz minz maxy miny minx maxx minx maxx maxz minz)))
	(MPP (if (or (< ox minx) (> oy maxy) (> oz maxz))
		 nil
		 (big-not-or miny maxy maxz minz miny maxy minx maxx maxx minx maxz minz)))
	(PMM (if (or (> ox maxx) (< oy miny) (< oz minz))
		 nil
		 (big-not-or maxy miny minz maxz maxy miny maxx minx minx maxx minz maxz)))
	(PMP (if (or (> ox maxx) (< oy miny) (> oz maxz))
		 nil
		 (big-not-or maxy miny minz maxz miny maxy maxx minx maxx minx minz maxz)))
	(PPM (if (or (> ox maxx) (> oy maxy) (< oz minz))
		 nil
		 (big-not-or maxy miny minz maxz maxy miny minx maxx minx maxx maxz minz)))
	(PPP (if (or (> ox maxx) (> oy maxy) (> oz maxz))
		 nil
		 (big-not-or maxy miny minz maxz miny maxy minx maxx maxx minx maxz minz)))))))

(defun aab-plucker-intersect-p (ox oy oz dx dy dz minx miny minz maxx maxy maxz)
  ;; Algorithm from JGT
  ;; Only returns T or NIL for intersection
  (declare (optimize (speed 3) (debug 0)))
  (declare (single-float ox oy oz dx dy dz minx miny minz maxx maxy maxz))
  ;; we need a couple macros for saving on the typing
  ;; yes it's ugly but the C++ from
  ;; TODO: can the macros be taken further to make more succinct?
  (macrolet ((with-distances (&body body)
	       `(let ((xa (- minx ox))
		      (ya (- miny oy))
		      (za (- minz oz))
		      (xb (- maxx ox))
		      (yb (- maxy oy))
		      (zb (- maxz oz)))
		  (declare (single-float xa ya za xb yb zb))
		  ,@body)))
    (labels ((big-not-or (a b c d e f g h i j k l)
	       (declare (single-float a b c d e f g h i j k l))
	       (not (or (minusp (- (* dx a) (* dy g)))
			(plusp (- (* dx b) (* dy h)))
			(plusp (- (* dx c) (* dz i)))
			(minusp (- (* dx d) (* dz j)))
			(minusp (- (* dy e) (* dz k)))
			(plusp (- (* dy f) (* dz l)))))))
      (if (minusp dx)
	  (if (minusp dy)
	      (if (minusp dz)
		  (if (or (< ox minx) (< oy miny) (< oz minz)) ;MMM
		      nil
		      (with-distances (big-not-or ya yb zb za za zb xb xa xa xb yb ya)))
		  (if (or (< ox minx) (< oy miny) (> oz maxz)) ;MMP
		      nil
		      (with-distances (big-not-or ya yb zb za za zb xb xa xb xa ya yb))))
	      (if (minusp dz)
		  (if (or (< ox minx) (> oy maxy) (< oz minz)) ;MPM
		      nil
		      (with-distances (big-not-or ya yb zb za zb za xa xb xa xb yb ya)))
		  (if (or (< ox minx) (> oy maxy) (> oz maxz)) ;MPP
		      nil
		      (with-distances (big-not-or ya yb zb za zb za xa xb xb xa ya yb)))))
	  (if (minusp dy)
	      (if (minusp dz)
		  (if (or (> ox maxx) (< oy miny) (< oz minz)) ;PMM
		      nil
		      (with-distances (big-not-or yb ya za zb za zb xb xa xa xb yb ya)))
		  (if (or (> ox maxx) (< oy miny) (> oz maxz)) ;PMP
		      nil
		      (with-distances (big-not-or yb ya za zb za zb xb xa xb xa ya yb))))
	      (if (minusp dz)
		  (if (or (> ox maxx) (> oy maxy) (< oz minz)) ;PPM
		      nil
		      (with-distances (big-not-or yb ya za zb zb za xa xb xa xb yb ya)))
		  (if (or (> ox maxx) (> oy maxy) (> oz maxz)) ;PPP
		      nil
		      (with-distances (big-not-or yb ya za zb zb za xa xb xb xa ya yb)))))))))

(defmethod intersect-p ((ray ray3d) (aabox bounding-box))
  (declare (inline aab-plucker-intersect-p))
  (with-slots ((origin origin) (dir direction))
      ray
    (with-slots ((minb min-point) (maxb max-point))
	aabox
      (let ((ox (aref origin 0))
	    (oy (aref origin 1))
	    (oz (aref origin 2))
	    (dx (aref dir 0))
	    (dy (aref dir 1))
	    (dz (aref dir 2))
	    (minx (aref minb 0))
	    (miny (aref minb 1))
	    (minz (aref minb 2))
	    (maxx (aref maxb 0))
	    (maxy (aref maxb 1))
	    (maxz (aref maxb 2))
	    )
	(aab-plucker-intersect-p ox oy oz dx dy dz minx miny minz maxx maxy maxz)))))

(defmethod intersect-p ((ray plucker-ray) (aabox bounding-box))
  (with-slots ((origin origin)
	       (dir direction)
	       r0 r1 r3 category)  ray
    (with-slots ((minb min-point)
		 (maxb max-point))  aabox
      (declare (inline aab-plucker-cff-intersect-p))
      ;      (ray-box-intersect origin dir minb maxb)
      (aab-plucker-cff-intersect-p category origin dir r0 r1 r3 minb maxb))))

(defmethod intersect-p ((obj1 t) (obj2 t))
  ;; Do nothing when a intersect method hasn't been defined.
  (values))

(defun line-box-test (x y z)
  (declare (single-float x y z))
  (let ((ray (make-ray-from-line (v3d:vec3 3.0 3.0 0.0) (v3d:vec3 x y z)))
	(box (make-bbox (v3d:vec3 2 2 2) (v3d:vec3 4 4 4))))
    (intersect-p ray box)))


(defun ray-box-test (x y z)
  (declare (single-float x y z))
  (let ((ray (make-ray (v3d:vec3 3.0 3.0 0.0) (v3d:normalize! (v3d:vec3 x y z))))
	(box (make-bbox (v3d:vec3 2 2 2) (v3d:vec3 4 4 4))))
    (intersect-p ray box)))


(defun run-ray-tests (n rayfn)
  (loop repeat n
     with box = (make-bbox (v3d:vec3 2 2 2) (v3d:vec3 4 4 4))
     for ray  = (funcall rayfn (v3d::random-vector 5.0) (v3d:normalize! (v3d::random-vector 5.0)))
     for test = (intersect-p ray box)
     if test count it))

#||
Given ray:

    * R0 = (0, 4, 2)
    * Rd = (0.213, -0.436, 0.873) 

Box:

    * Bl = (-1, 2, 1)
    * Bh = (3, 3, 3) 

||#