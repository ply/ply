(defsystem ply
  :description "A simple viewer to load and view A|w OBJ files"
  :version "0.0.1" 
  :depends-on (cl-glut cl-glu)
  :serial t
  :components
  ((:file "package")
   (:file "va-vbo")
   (:file "vector-3d")
   (:file "cl-obj")
   (:file "scene")
   (:file "object-3d")
   (:file "bounding-volumes")
   (:file "mesh")
   (:file "rays")
   (:file "lights")
   (:file "camera")
   (:file "render")
   (:file "import")
   (:file "menu")
   (:file "view")
   (:file "input-interface-new")))