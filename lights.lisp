;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


;;;  lights.lisp
;;;
;;; Provide CLOS interface to OpenGL lights
;;; 
;;; Copyright (c) 2008, Charlie McMackin
;;;

(in-package #:ply)

(defclass simple-light (world-object)
  ((light-num     
      :initarg :number     :accessor light-num)
   (diffuse     
      :initarg :diffuse    :accessor diffuse    :type v3d:vector4d)
   (ambient     
      :initarg :ambient    :accessor ambient    :type v3d:vector4d)
   (specular 
      :initarg :specular   :accessor specular   :type v3d:vector4d)
   (attenuation)
   (attenuation-range))
  (:default-initargs 
   :number :light0
    :diffuse (v3d:vec4 1.0 1.0 1.0 1.0) 
    :ambient (v3d:vec4 0.0 0.0 0.0 1.0)
    :specular (v3d:vec4 1.0 1.0 1.0 1.0)))


;; Should specific types of lights have a class or should they have "type" slot to set?
(defclass point-light (simple-light)
  ((light-position 
      :initarg :light-position :accessor light-position :type v3d:vector4d))
  (:default-initargs :light-position (v3d:vec4 0.0 0.0 0.0 1.0)))

(defclass directional-light (simple-light)
  ((light-direction 
      :initarg :direction :accessor light-direction :type v3d:vector4d))
  (:default-initargs :direction (v3d:vec4 1.0 0.0 0.0 0.0))) 

(defclass spot-light (point-light)
  ((spot-direction 
      :initarg :spot-direction :accessor spot-direction :type v3d:vector3d)
   (cutoff-angle   
      :initarg :cutoff   :type single-float)
   (spot-exponent  
      :initarg :exponent :type single-float)
   (fall-off       
      :initarg :fall-off :type single-float))
  (:default-initargs :spot-direction (v3d:vec3 0.0 0.0 1.0)))

#+ignore (defclass point-light (point-light-mixin simple-light)
  ()
  (:documentation 
   "A Point Light Object, emits light in all directions"))
#+ignore
(defclass directional-light (directional-light-mixin simple-light)
  ()
  (:documentation 
   "A Directional Light Object emits light in one direction from negative 
infinity to positive infinity"))
#+ignore (defclass spot-light (spot-light-mixin simple-light)
  ()
  (:documentation 
   "A Spot light emits light in a cone from its position in a certain direction"))


(defun make-light (light-type)
  "Make a light object. LIGHT-TYPE should be :point :directional or :spot"
  (make-instance (ecase light-type
		   (:point 'point-light)
		   (:directional 'directional-light)
		   (:spot 'spot-light))))




(defparameter *default-lights* nil
  "A list container for light objects. the first three cells are for the default
 one/two-lighting")


(setf *default-lights* 
      (let ((light-0 (make-light :point))
	    (light-1 (make-instance 'directional-light
				    :number :light0
				    :direction (v3d:vec4 -0.70710677 -0.70710677 0.0 0.0)))
	    (light-2 (make-instance 'directional-light
				    :number :light1
				    :direction (v3d:vec4 0.70710677 0.70710677 0.0 0.0))))
	(list light-0 light-1 light-2)))

(defparameter *scene-lights* nil
  "A list container for user-defined light objects")
(defparameter *default-lighting* 1
  "Values 0,1, and 2 are used to control default-lighting scheme")


(defun set-lighting ()
  (draw (ecase *default-lighting*
	  (0  *scene-lights*)
	  (1  (first *default-lights*))
	  (2  (rest *default-lights*)))))

(defun toggle-default-lighting ()
  (setf *default-lighting* 
	(case *default-lighting*
	  (0 1) (1 2) (2 1)))
  (set-lighting)
  (glut:post-redisplay))

#+ignore
(defmacro with-lights (light-list &body body)
  ;; FIXME: needs thinking

  (assert (every #'(lambda (x)
		   (typep x light)) light-list))
  `(progn 
     ,@(loop 
	 for i from 0
	 for light in light-list
	 for light-id = (gl:symbolicate (concatenate ":light" i))
	 collect `(light-on ,light-id ,light))
  
      ,@body))




;;; Materials section -- to be put in it's own file

(defclass simple-material ()
  (diffuse
   ambient
   specular
   shininess
   emissive))

(defmethod draw ((material simple-material))
  (gl:color-material :front :ambient-and-diffuse))
