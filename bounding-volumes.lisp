;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)

(defclass bounding-box ()
  ((min-point :accessor min-point :initarg :min-point :type v3d:vector3d)
   (max-point :accessor max-point :initarg :max-point :type v3d:vector3d)))

#+ignore
(defmethod draw :before ((box bounding-box))
  (gl:push-matrix))

#+ignore
(defmethod draw :after ((box bounding-box))
  (gl:pop-matrix))

(defmethod draw ((box bounding-box))
 ;; If a bounding box should be displayed, we do it after everything else.
  (declare (ignore box))
  (values))

(defmethod draw :after ((box bounding-box))
  (with-slots ((min min-point) (max max-point)) box
    (let ((lx (aref min 0)) (ly (aref min 1)) (lz (aref min 2))
	  (hx (aref max 0)) (hy (aref max 1)) (hz (aref max 2)))
      ;;hard coded vertex winding order for faces
      (let ((box-verts (make-array 24 
				   :element-type 'single-float 
				   :initial-contents (list lx ly lz lx hy lz hx hy lz hx ly lz
							   lx ly hz lx hy hz hx hy hz hx ly hz)))
	    (box-indices (make-array 24
				     :element-type 'fixnum
				     :initial-contents (append  '(0 3 2 1) '(1 5 4 0) '(0 4 7 3)
								'(3 7 6 2) '(2 6 5 1) '(5 6 7 4)))))
	
	(gl:disable :lighting)
	(gl:polygon-mode :front-and-back :line)
	(gl:line-width 1.0)
	
	(gl:with-primitives :quads
	  (loop for i across box-indices
	     for k = (* i 3)
	     do (apply #'gl:vertex
		       (loop for j from k below (+ 3 k)
			  collect (aref box-verts j)))))))))

(defun make-bbox (min-bound max-bound)
  (declare (type v3d:vector3d min-bound max-bound))
  (make-instance 'bounding-box :min-point min-bound :max-point max-bound))

(defun find-bounds (array)
  (loop for vertex across array
     for x = (aref vertex 0)
     for y = (aref vertex 1)
     for z = (aref vertex 2)
     maximize x into max-x
     minimize x into min-x
     maximize y into max-y
     minimize y into min-y
     maximize z into max-z
     minimize z into min-z
     finally (return (values (list min-x min-y min-z)
			     (list max-x max-y max-z)))))

;; TODO: make it compute-bounding-volume (volume-type (obj <class>))
(defmethod compute-bounding-volume ((obj cl-obj:cl-obj))
  (multiple-value-bind (min-b max-b)
      (find-bounds (cl-obj:vertices obj))
    (make-bbox (apply #'v3d:vec3 min-b) (apply #'v3d:vec3 max-b))))



