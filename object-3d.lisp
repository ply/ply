;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)

(defclass world-object () 
  ((id :accessor id :initarg :id)
   (name :accessor name :initarg :name)))

(defclass transform ()
  ((translation :accessor translation :initarg :trans)
   (rotation :accessor rotation :initarg :rot)
   (scale :accessor scale :initarg :scale))
  (:default-initargs :trans (v3d:vec3-0)
    :rot (v3d:vec3-0)
    :scale (v3d:vec3 1.0 1.0 1.0)))

(defclass object-state ()
  ((wireframep :accessor wireframep :initarg :wire)
   (solidp :accessor solidp :initarg :solid)
   (hiddenp :accessor hiddenp :initarg :hidden)
   (highlightp :accessor highlightp :initform nil))
  (:default-initargs :wire nil :solid t :hidden nil))





#||
(defclass material (world-object)
  ())

(defclass shader (world-object)
  ())

(defclass group (transform world-object)
  (children))

(defclass texture (world-object)
  (texture-file))
||#

(defgeneric toggle-wireframe (object)
  (:documentation "Toggles wireframe display on and off"))
(defgeneric toggle-solid (object)
  (:documentation "Toggles solid display on and off"))
(defgeneric toggle-hide (object)
  (:documentation "Toggles object's visibility"))

(defgeneric compute-bounding-volume (object)
  (:documentation "Compute and store the max and min bound of the object's space"))

(defgeneric draw (object)
  (:documentation "The main function to draw everything visible in a 3D scene"))


#||
(defmethod parent ())
(defmethod unparent ())
(defmethod translate ())
(defmethod rotate ())
(defmethod scale ())
||#