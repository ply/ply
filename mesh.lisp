;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)

(defun flatten-array (array)
  ;;TODO: put this in a util file (?)
  (loop with size = (* (length (aref array 0)) (length array))
     with result = (make-array size :fill-pointer 0)
     for old across array
     do (loop for val across old
	   do (vector-push val result))
     finally (return result)))

(defclass mesh (transform object-state bounding-box world-object)
  ((vertex-array :accessor vertex-array :initarg :v) 
   (vertex-normals :accessor vertex-normals :initarg :vn)
   (face-indices :accessor face-indices :initarg :f)
   (uv-coords :accessor uv-coords :initarg :uv))
  (:default-initargs :v nil :vn nil :f nil :uv nil)
  (:documentation "an object that has slots for the basic elements of a 3D mesh:
     vertex-array : array of 3-space vertices
     vertex-normals : array of unit normals for each vertex in vertex-array
     face-indices : array of indices of VERTEX-ARRAY that make up each facet 
     uv-coords : array of UV coordinates for texturing"))
(defclass standard-mesh (mesh) 
  ()
  (:documentation "a mesh that is drawn with slow GL functions like
gl:vertex and gl:normal"))

(defclass vbo-mesh (mesh)
  ((vertex-vbo :accessor vertex-vbo :initarg :vertex-vbo)
   (norm-vbo :accessor norm-vbo :initarg :norm-vbo)
   (index-vbo :accessor index-vbo :initarg :index-vbo))
  (:default-initargs :vertex-vbo nil :norm-vbo nil :index-vbo nil)
  (:documentation "A MESH that will render with Vertex Array Objects or Vertex
 Buffer Objects"))


;; (defun mesh-to-vbo-mesh (mesh gl-type target usage))
(defun mesh-to-vbo-mesh (mesh)
  (with-slots (vertex-array vertex-normals face-indices) mesh
    (let ((v-vao (flatten-array vertex-array))
	  (n-vao (flatten-array vertex-normals))
	  (f-vao (flatten-array face-indices)))
      (change-class mesh 
		    'vbo-mesh 
		    :vertex-vbo 
		    (make-vao (length (aref vertex-array 0)) 
			      0 :float v-vao)
		    :norm-vbo 
		    (make-vao (length (aref vertex-normals 0))
			      0 :float n-vao)
		    :index-vbo 
		    (make-vao (length (aref face-indices 0))
			      0 :unsigned-short f-vao)))))


(defmethod shared-initialize :after ((obj mesh) slot-names &key)
  (declare (ignore slot-names))
  (compute-bounding-volume obj))

(defmethod compute-bounding-volume ((obj mesh))
  (with-slots (min-point max-point) obj
    (multiple-value-bind (min-b max-b)
	(find-bounds (vertex-array obj))
      (setf min-point (apply #'v3d:vec3 min-b)  ;coerce?
	    max-point (apply #'v3d:vec3 max-b)))))


