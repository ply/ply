;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;

;; Initial file for simplistic scene lists. Initially done as a linked list.
;; All objects in the scene-list are expected to have a method DRAW

(in-package #:ply)

(defparameter *scene* nil
  "A list container for selectable objects") 
(defparameter *background* nil
  "A list container for non-selectable objects")
(defparameter *selection* nil
  "A list container for objects that have been selected")

(defmacro add-to-scene (scene object)
  `(push ,object ,scene))

(defun select (object)
  (setf (highlightp object) t)
  (pushnew object *selection*))

(defun selected-p (object)
  (member object *selection*))

(defun deselect (object)
  (setf (highlightp object) nil)
  ;;seems that delete is not being as destuctive
  ;;as I want with a list
  (setf *selection* (delete object *selection*)))

(defun clear-selections ()
  "Clears all selections"
  (loop for object in *selection*
     do (setf (highlightp object) nil))
  (setf *selection* nil)
  (glut:post-redisplay))

(defun delete-selections ()
  "Removes the objects from the scene"
  (when *selection*
    #+ignore (setf *scene* (delete-if #'(lambda (x) (member x *selection*)) *scene*))
    (setf *scene* (nset-difference *scene* *selection*))
    (clear-selections)))

(defun add-selection (x y camera)
  "Casts a ray and tests for intersection. All intersected objects are selected"
  (loop 
     with ray = (get-pick-ray3 x y camera)
     for object in *scene* 
     when (intersect-p ray object)
     do (if (selected-p object)
	    (deselect object)
	    (select object))
     finally (glut:post-redisplay)))