;; 
;;
;; Copyright (c) 2008, Charlie McMackin
;;
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are met:
;;
;;   1. Redistributions of source code must retain the above copyright notice,
;;      this list of conditions and the following disclaimer.
;;   2. Redistributions in binary form must reproduce the above copyright notice,
;;      this list of conditions and the following disclaimer in the documentation
;;	and/or other materials provided with the distribution.
;;   3. The name of the author may not be used to endorse or promote products
;;   	derived from this software without specific prior written permission.
;;
;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
;; WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
;; MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
;; EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
;; PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
;; BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
;; IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
;; ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
;; POSSIBILITY OF SUCH DAMAGE.
;;


(in-package #:ply)

#||(defun view-from-axis (axis) 
  (setf *view-tx* 0
	*view-ty* 0
	*view-tz* 0)
  (ecase axis
    (x (setf *view-tx* 1))
    (y (setf *view-ty* -1))
    (z (setf *view-tz* 1))))
||#

(defun view-along-axis (axis)
  (gl:load-identity)
  (let ((xc 0.0)
	(yc 0.0)
	(zc 0.0))
    (ecase axis
      (x (setf xc 10.0))
      (y (setf yc 10.0))
      (z (setf zc 10.0)))
    (glu:look-at xc yc zc 0 0 0 0 1 0)))

;; dirty hack
(defvar *smooth* t)
(defun toggle-smooth ()
  (setf *smooth* (not *smooth*))
  (if *smooth*
      (gl:shade-model :smooth)
      (gl:shade-model :flat))
  (glut:post-redisplay))

(defmethod glut:keyboard ((w gl-window) key x y)
  (with-slots ((scam save-camera) (vcam view-camera)) w
    (case key
      (#\q (when (movingp vcam)
	     (setf (click-x scam) x
		   (click-y scam) y)
	     (toggle-mode vcam)
	     (set-camera scam vcam)))
      (#\Esc (glut:leave-main-loop))
      (#\Tab (toggle-smooth))
      (#\Delete (delete-selections))
      (#\Space (clear-selections))
      (#\o (toggle-ortho vcam))
      (#\x (view-along-axis 'x) (glut:post-redisplay))
      (#\y (view-along-axis 'y) (glut:post-redisplay))
      (#\z (view-along-axis 'z) (glut:post-redisplay))
      (#\w (if *smooth* (toggle-wireframe *scene*) (toggle-solid *scene*)))
      (#\h (toggle-hide *scene*))
      (#\l (toggle-default-lighting)))))
  
(defmethod glut:mouse ((w gl-window) button state x y)
;  (format t "~&Button value is ~a ~%Pressed value is ~a~%" button state)
  (with-slots ((scam save-camera) (vcam view-camera)) w
    (cond ((and (eq button :middle-button) (eq state :up))
	   (unless (movingp vcam)
	     (glut:set-cursor :cursor-none)
	     (setf (movingp vcam) t
		   (camera-mode vcam) 'orbit
		   (click-x scam) x
		   (click-y scam) y)
	     (set-camera scam vcam)))

	  ((and (eq button :left-button) (eq state :down))
	   (if (movingp vcam) 
	       (progn (glut:set-cursor :cursor-left-arrow)
		      (setf (movingp vcam) nil))
	       (add-selection x (- (glut::height w) y) scam)
	       ))

	  ((and (eq button :right-button) (eq state :down))
	   ;; Return to original view
	   (if (movingp vcam) 
	       (progn (setf (movingp vcam) nil)
		      (set-camera vcam scam)
		      (glut:post-redisplay))
	       ;	       (get-pick-ray x y)
	       ))

	  ((eq button :wheel-down)
	   (dolly-zoom vcam -1.0) (glut:post-redisplay)
	   )

	  ((eq button :wheel-up)
	   (dolly-zoom vcam 1.0) (glut:post-redisplay)
	   ))))


#||
;; defining a mouse-wheel func causes odd and non-intuitive things to happen
;; with the normal mouse func inside freeglut
(defmethod glut:mouse-wheel ((w gl-window) button pressed x y)
  (declare (ignore x y))
  (format t "~&Button value is ~a ~%Pressed value is ~a~%" button pressed)
  (if (eq pressed :up)
      (dolly-zoom (view-camera w) 1.0)
      (dolly-zoom (view-camera w) -1.0))
  (glut:post-redisplay))
||#

(defmethod glut:motion ((w gl-window) x y)
 (with-slots ((scam save-camera) (vcam view-camera)) w
   (when (movingp vcam)
     (let ((delta-sign (if (> x (click-x scam)) 1.0 -1.0)))
      (dolly-zoom vcam  (* delta-sign 0.1)))
     (glut:post-redisplay)
     (setf (click-x scam) x   (click-y scam) y))))

(defmethod glut:passive-motion ((w gl-window) x y)  
  (with-slots ((scam save-camera) (vcam view-camera)) w
    (when (movingp vcam) 
      (ecase (camera-mode vcam)
	(orbit (orbit vcam scam x y))
	;	(track (track vcam scam x y))
	(track (pan vcam scam x y)))
      (glut:post-redisplay))))


